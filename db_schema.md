````sql
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 10.1

-- Started on 2018-03-14 15:41:44 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13308)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3060 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 16424)
-- Name: categories; Type: TABLE; Schema: public; Owner: awsmaster
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying(30)
);


ALTER TABLE categories OWNER TO awsmaster;

--
-- TOC entry 188 (class 1259 OID 16427)
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: awsmaster
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO awsmaster;

--
-- TOC entry 3061 (class 0 OID 0)
-- Dependencies: 188
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: awsmaster
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- TOC entry 185 (class 1259 OID 16401)
-- Name: pictures; Type: TABLE; Schema: public; Owner: awsmaster
--

CREATE TABLE pictures (
    id integer NOT NULL,
    category_id integer,
    title character varying,
    description character varying,
    url character varying NOT NULL,
    thumbnail_url character varying,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    activ boolean DEFAULT false,
    front_page boolean DEFAULT false
);


ALTER TABLE pictures OWNER TO awsmaster;

--
-- TOC entry 186 (class 1259 OID 16407)
-- Name: pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: awsmaster
--

CREATE SEQUENCE pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pictures_id_seq OWNER TO awsmaster;

--
-- TOC entry 3062 (class 0 OID 0)
-- Dependencies: 186
-- Name: pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: awsmaster
--

ALTER SEQUENCE pictures_id_seq OWNED BY pictures.id;


--
-- TOC entry 2930 (class 2604 OID 16430)
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: awsmaster
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- TOC entry 2927 (class 2604 OID 16409)
-- Name: pictures id; Type: DEFAULT; Schema: public; Owner: awsmaster
--

ALTER TABLE ONLY pictures ALTER COLUMN id SET DEFAULT nextval('pictures_id_seq'::regclass);


--
-- TOC entry 2934 (class 2606 OID 16434)
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: awsmaster
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- TOC entry 2932 (class 2606 OID 16432)
-- Name: pictures pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: awsmaster
--

ALTER TABLE ONLY pictures
    ADD CONSTRAINT pictures_pkey PRIMARY KEY (id);


--
-- TOC entry 2935 (class 2606 OID 16435)
-- Name: pictures pictures_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: awsmaster
--

ALTER TABLE ONLY pictures
    ADD CONSTRAINT pictures_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- TOC entry 3059 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: awsmaster
--

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO awsmaster;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-03-14 15:41:51 CET

--
-- PostgreSQL database dump complete
--
````
